import { calculateBonuses }  from "./bonus-system.js";

describe('Standard program (0.05)', () => {
  let program = 'Standard'

  test('amount < 10000 (1)',  () => {
    let amount = 9000
    expect(calculateBonuses(program, amount)).toEqual(0.05*1);
  });

  test('amount = 10000 (1.5)',  () => {
    let amount = 10000
    expect(calculateBonuses(program, amount)).toEqual(0.05*1.5);
  });

  test('amount < 50000 (1.5)',  () => {
    let amount = 20000
    expect(calculateBonuses(program, amount)).toEqual(0.05*1.5);
  });

  test('amount = 50000 (2)',  () => {
    let amount = 50000
    expect(calculateBonuses(program, amount)).toEqual(0.05*2);
  });

  test('amount < 100000 (2)',  () => {
    let amount = 90000
    expect(calculateBonuses(program, amount)).toEqual(0.05*2);
  });

  test('amount = 100000 (2.5)',  () => {
    let amount = 100000
    expect(calculateBonuses(program, amount)).toEqual(0.05*2.5);
  });

  test('amount > 100000 (2.5)',  () => {
    let amount = 110000
    expect(calculateBonuses(program, amount)).toEqual(0.05*2.5);
  });

  test('unknown (2.5)',  () => {
    let amount = 'unknown'
    expect(calculateBonuses(program, amount)).toEqual(0.05*2.5);
  });
});

describe('Premium program (0.1)', () => {
  let program = 'Premium'

  test('amount < 10000 (1)',  () => {
    let amount = 9000
    expect(calculateBonuses(program, amount)).toEqual(0.1*1);
  });

  test('amount = 10000 (1.5)',  () => {
    let amount = 10000
    expect(calculateBonuses(program, amount)).toEqual(0.1*1.5);
  });

  test('amount < 50000 (1.5)',  () => {
    let amount = 20000
    expect(calculateBonuses(program, amount)).toEqual(0.1*1.5);
  });

  test('amount = 50000 (2)',  () => {
    let amount = 50000
    expect(calculateBonuses(program, amount)).toEqual(0.1*2);
  });

  test('amount < 100000 (2)',  () => {
    let amount = 90000
    expect(calculateBonuses(program, amount)).toEqual(0.1*2);
  });

  test('amount = 100000 (2.5)',  () => {
    let amount = 100000
    expect(calculateBonuses(program, amount)).toEqual(0.1*2.5);
  });

  test('amount > 100000 (2.5)',  () => {
    let amount = 110000
    expect(calculateBonuses(program, amount)).toEqual(0.1*2.5);
  });

  test('unknown (2.5)',  () => {
    let amount = 'unknown'
    expect(calculateBonuses(program, amount)).toEqual(0.1*2.5);
  });
});

describe('Diamond program (0.2)', () => {
  let program = 'Diamond'

  test('amount < 10000 (1)',  () => {
    let amount = 9000
    expect(calculateBonuses(program, amount)).toEqual(0.2*1);
  });

  test('amount = 10000 (1.5)',  () => {
    let amount = 10000
    expect(calculateBonuses(program, amount)).toEqual(0.2*1.5);
  });

  test('amount < 50000 (1.5)',  () => {
    let amount = 20000
    expect(calculateBonuses(program, amount)).toEqual(0.2*1.5);
  });

  test('amount = 50000 (2)',  () => {
    let amount = 50000
    expect(calculateBonuses(program, amount)).toEqual(0.2*2);
  });

  test('amount < 100000 (2)',  () => {
    let amount = 90000
    expect(calculateBonuses(program, amount)).toEqual(0.2*2);
  });

  test('amount = 100000 (2.5)',  () => {
    let amount = 100000
    expect(calculateBonuses(program, amount)).toEqual(0.2*2.5);
  });

  test('amount > 100000 (2.5)',  () => {
    let amount = 110000
    expect(calculateBonuses(program, amount)).toEqual(0.2*2.5);
  });

  test('unknown (2.5)',  () => {
    let amount = 'unknown'
    expect(calculateBonuses(program, amount)).toEqual(0.2*2.5);
  });
});

describe('Unknown program (0)', () => {
  let program = 'Unknown'

  test('amount < 10000 (1)',  () => {
    let amount = 9000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('amount = 10000 (1.5)',  () => {
    let amount = 10000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('amount < 50000 (1.5)',  () => {
    let amount = 20000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('amount = 50000 (2)',  () => {
    let amount = 50000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('amount < 100000 (2)',  () => {
    let amount = 90000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('amount < 100000 (2.5)',  () => {
    let amount = 100000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('amount > 100000 (2.5)',  () => {
    let amount = 110000
    expect(calculateBonuses(program, amount)).toEqual(0);
  });

  test('unknown (2.5)',  () => {
    let amount = 'unknown'
    expect(calculateBonuses(program, amount)).toEqual(0);
  });
});